<?php

class SearchController extends Controller {

    /**
     * @var string index dir as alias path from <b>application.</b>  , default to <b>runtime.search</b>
     */
    private $_indexFiles = 'runtime.search';

//    /**
//     * (non-PHPdoc)
//     * @see CController::init()
//     */
//    public function init() {
//        Yii::import('application.vendors.*');
//        require_once('Zend/Search/Lucene.php');
//        parent::init();
//    }

    public function actionIndex() {
        $model = new Book('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Book']))
            $model->attributes = $_GET['Book'];
        $this->render('index', array(
            'model' => $model,
        ));
    }

    public function actionSearch() {

        $this->layout = 'column2';
        if (($term = Yii::app()->getRequest()->getParam('q', null)) !== null) {
//            $index = new Zend_Search_Lucene(Yii::getPathOfAlias('application.' . $this->_indexFiles));
//            $results = $index->find($term);
//            $query = Zend_Search_Lucene_Search_QueryParser::parse($term);
            $results = Book::model()->search();
            $this->render('search', compact('results', 'term'));
        }
    }

}