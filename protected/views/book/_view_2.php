<div class="view">

	<b><?php echo CHtml::encode("Book ISBN"); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data['id']), array('view', 'id'=>$data['id'])); ?>
	<br />

	<b><?php echo CHtml::encode("Book Title"); ?>:</b>
	<?php echo CHtml::encode($data['title']); ?>
	<br />

	<b><?php echo CHtml::encode("Author Name"); ?>:</b>
	<?php echo CHtml::encode($data["authorName"]); ?>
	<br />

	<b><?php echo CHtml::encode("Publisher Name"); ?>:</b>
	<?php echo CHtml::encode($data["publisherName"]); ?>
	<br />

	<b><?php echo CHtml::encode("Year"); ?>:</b>
	<?php echo CHtml::encode($data["year"]); ?>
	<br />

	<b><?php echo CHtml::encode("Book Genre"); ?>:</b>
	<?php echo CHtml::encode($data["genreName"]); ?>
	<br />

	<b><?php echo CHtml::encode("Book description"); ?>:</b>
	<?php echo CHtml::encode($data["description"]); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('conditionId')); ?>:</b>
	<?php echo CHtml::encode($data->conditionId); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?>:</b>
	<?php echo CHtml::encode($data->quantity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	*/ ?>

</div>