<?php
$this->breadcrumbs=array(
	'Books'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'List Book', 'url'=>array('index')),
	array('label'=>'Create Book', 'url'=>array('create'), 'visible'=>!Yii::app()->user->isGuest),
	array('label'=>'Update Book', 'url'=>array('update', 'id'=>$model->id), 'visible'=>!Yii::app()->user->isGuest),
	array('label'=>'Delete Book', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'), 'visible'=>!Yii::app()->user->isGuest),
	array('label'=>'Manage Book', 'url'=>array('admin'), 'visible'=>!Yii::app()->user->isGuest)
);
?>

<h1>View Book #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
//		'authorId',
                array(               // related Author Name displayed as a link
                'label'=>'Author Name',
                'type'=>'raw',
                'value'=>CHtml::link(CHtml::encode($model->author->fullName),
                                     array('author/view','id'=>$model->author->id)),
                ),
//		'publisherId',
                array(               // related publisher displayed as a link
                'label'=>'Publisher',
                'type'=>'raw',
                'value'=>CHtml::link(CHtml::encode($model->publisher->name),
                                     array('publisher/view','id'=>$model->publisher->id)),
                ),
		'year',
//		'genraId',
                array(               // related Book Genre displayed as a link
                'label'=>'Book Genre',
                'type'=>'raw',
                'value'=>CHtml::link(CHtml::encode($model->genre->name),
                                     array('genre/view','id'=>$model->genre->id)),
                ),
		'description',
//		'conditionId',
                array(               // related Book Condition displayed as a link
                'label'=>'Book Condition',
                'type'=>'raw',
                'value'=>CHtml::link(CHtml::encode($model->condition->name),
                                     array('condition/view','id'=>$model->condition->id)),
                ),
		'quantity',
		'price',
	),
)); ?>
