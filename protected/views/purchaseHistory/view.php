<?php
$this->breadcrumbs=array(
	'Purchase Histories'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PurchaseHistory', 'url'=>array('index')),
	array('label'=>'Create PurchaseHistory', 'url'=>array('create')),
	array('label'=>'Update PurchaseHistory', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PurchaseHistory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PurchaseHistory', 'url'=>array('admin')),
);
?>

<h1>View Sales #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
//		'customerId',                
                array(               // to show the customer name insted of id
                    'label'=>'Customer Name',
                    'type'=>'raw',
                    'value'=>CHtml::link(CHtml::encode($model->customer->fullName),
                                         array('customer/view','id'=>$model->customerId)),
                ),
//		'bookId',                
                array(               // to show the book name insted of id
                    'label'=>'Book Name',
                    'type'=>'raw',
                    'value'=>CHtml::link(CHtml::encode($model->book->title),
                                         array('book/view','id'=>$model->bookId)),
                ),
//		'sellerId',                
                array(               // to show the seller name insted of id
                    'label'=>'Employee Name',
                    'type'=>'raw',
                    'value'=>CHtml::link(CHtml::encode($model->seller->fullName),
                                         array('employee/view','id'=>$model->sellerId)),
                ),
		'quantity',
		'price',
	),
)); ?>
