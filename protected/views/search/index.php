<?php 
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('book-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<div class="search-form" style="display:none">
</div><!-- search-form -->
<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'book-grid',
	'dataProvider'=>  $model->search(),
	'filter'=>$model,
	'columns'=>array(
//		'id',
		'title',                
//                array(               // related Book displayed as a link
//                  'name'=>'title',
//                  'value'=>$model->title,
//                ),
//		'authorId',
//		'publisherId',
//		'year',
//		'genraId',
		/*
		'description',
		'conditionId',
		'quantity',
		'price',
		*/
//		array(
//			'class'=>'CButtonColumn',
//		),
	),
)); ?>