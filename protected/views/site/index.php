<?php $this->pageTitle=Yii::app()->name; ?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>
<p>This “Bookstore” is a database whose relations contain
information about the descriptions of all the books that are sold in a bookstore. The attributes
associated to each particular book can be viewed by anyone from a convenient online interface
that is designed to accommodate an ease of accessibility. This database is designed to provide
logistics to small businesses and companies and contains a default interface that is used by
guests and a user-login interface for employees and super users.</p>
<p>
A guest user (using the default web interface) is allowed to search for and check availability of
a particular book. Employee can print to customers their purchase history which shows the
purchased book, date of purchase, and amount purchased. </p>
<p>
An employee or super user is allowed to search for books, delete books, insert new
books, and update books already in the database. An employee can view the purchase history
of any customer (with the assumption they aren’t abusing power or invading privacy).
</p>
<?php
if(Yii::app()->user->getIsAdmin())
    echo "Welcome " . YII::app()->user->getFullName() . "<br />";